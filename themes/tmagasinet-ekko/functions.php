<?php

 define('THEMEPATH', get_stylesheet_directory());

 add_action( 'wp_enqueue_scripts', 'kd_enqueue_parent_theme_style', 5 );
 if ( ! function_exists( 'kd_enqueue_parent_theme_style' ) ) {
     function kd_enqueue_parent_theme_style() {
         wp_enqueue_style( 'bootstrap' );
         wp_enqueue_style( 'keydesign-style', get_template_directory_uri() . '/style.css', array( 'bootstrap' ) );
         wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array('keydesign-style') );
     }
 }

 add_action( 'after_setup_theme', 'kd_child_theme_setup' );
 if ( ! function_exists( 'kd_child_theme_setup' ) ) {
     function kd_child_theme_setup() {
         load_child_theme_textdomain( 'ekko', get_stylesheet_directory() . '/languages' );
     }
 }

 // -------------------------------------
 // Edit below this line
 // -------------------------------------

require_once 'elements/elements.php';

// Fix for aws
function as3cf_filter_get_post_metadata( $metadata, $object_id, $meta_key, $single ) {
    $meta_filter = array('_wpb_shortcodes_custom_css', '_wpb_post_custom_css');
    if ( isset( $meta_key ) && $single && in_array($meta_key, $meta_filter)) {
        remove_filter( 'get_post_metadata', 'as3cf_filter_get_post_metadata', 100 );
        $metadata = get_post_meta( $object_id, $meta_key, $single );
        add_filter('get_post_metadata', 'as3cf_filter_get_post_metadata', 100, 4);
        $metadata = apply_filters( 'as3cf_filter_post_local_to_s3', $metadata );
    }
    return $metadata;
}
add_filter( 'get_post_metadata', 'as3cf_filter_get_post_metadata', 100, 4 );


// Add ssn for checkout
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
  unset($fields['billing']['billing_company']);
  return $fields;
}

add_action('admin_head', 'my_custom_admin_styles');

function my_custom_admin_styles() {
  // just add the css selectors below to hide each field as required
  echo '
    <style>
      #order_data .order_data_column .form-field._billing_company_field { display: none; }
    </style>
  ';
}